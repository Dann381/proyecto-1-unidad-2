import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class dlgwindow():


    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glade/glade.ui")

        self.dialogo = self.builder.get_object("dlg")
        self.dialogo.set_title("Información")
        self.dialogo.resize(300, 50)

        self.boton = self.builder.get_object("boton")
        self.boton.connect("clicked", self.salir)

        self.dialogo.show_all()

    def salir(self, btn=None):
        self.dialogo.destroy()


if __name__ == "__main__":
    MainWindow()
    Gtk.main()

