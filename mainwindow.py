from os import system
import pandas
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dlgwindow import dlgwindow
import dataobesidad
import obesidad_muertos
import alcohol_muertos
import topdesnutricion
import masmuertes


class MainWindow():


    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./glade/glade.ui")

        self.window = self.builder.get_object("window")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Ventana Principal")
        self.window.resize(800, 600)
        
        self.btn = self.builder.get_object("btn")
        self.btn.connect("clicked", self.abre_dialogo)

        self.label = self.builder.get_object("label")

        self.imagen = self.builder.get_object("imagen")
        self.pandastree = self.builder.get_object("pandas_tree")
        self.pandastree.connect("cursor-changed", self.cambio_cursor)

        self.comboboxtext = self.builder.get_object("comboboxtext")
        self.comboboxtext.set_entry_text_column(0)
        lista = ["¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad?",
                 "¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?",
                 "¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?",
                 "¿Cuáles son los 5 paı́ses que tienen más habitantes con desnutrición?",
                 "¿Cuáles son los 5 paı́ses que tienen más muertes por Covid-19?"]
        for index in lista:
            self.comboboxtext.append_text(index)
        self.comboboxtext.connect("changed", self.cambio_combobox)

        self.window.show_all()

    def cambio_combobox(self, cmb=None):
        if self.pandastree.get_columns():
            for column in self.pandastree.get_columns():
                self.pandastree.remove_column(column)
 
        it = self.comboboxtext.get_active()
        if it == 0:
            self.dataframe = dataobesidad.main()
        elif it == 1:
            self.dataframe = obesidad_muertos.main()
        elif it == 2:
            self.dataframe = alcohol_muertos.main()
        elif it == 3:
            self.dataframe = topdesnutricion.main()
        elif it == 4:
            self.dataframe = masmuertes.main()
        len_columns = len(self.dataframe.columns)
        self.model = Gtk.ListStore(*(len_columns * [str]))
        self.pandastree.set_model(model=self.model)
        cell = Gtk.CellRendererText()
        for i in range(len_columns):
            column = Gtk.TreeViewColumn(self.dataframe.columns[i],
                                        cell,
                                        text=i)
            self.pandastree.append_column(column)
        for value in self.dataframe.values:
            row = [str(i) for i in value]
            self.model.append(row)
        self.imagen.set_from_file("imagen.png")


    def cambio_cursor(self, tree=None):
        model, it = self.pandastree.get_selection().get_selected()
        if model is None or it is None:
            return
        
        lugar = self.comboboxtext.get_active()
        if lugar != 2:
            pais = model.get_value(it, 1)
            posicion = model.get_value(it, 0)
            self.label.set_text(f"Posición {posicion}: {pais}")
        else:
            pais1 = model.get_value(it, 1)
            pais2 = model.get_value(it, 3)
            posicion = model.get_value(it,0)
            self.label.set_text(f"Posición {posicion}: {pais1} y {pais2}")


    def abre_dialogo(self, btn=None):
        dlgwindow()


if __name__ == "__main__":
    MainWindow()
    Gtk.main()

