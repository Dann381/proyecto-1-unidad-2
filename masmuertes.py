import os
import pandas as pd
import matplotlib.pyplot as plt


def cargar_dataset():
    data = pd.read_csv("suministro_alimentos_kcal.csv")
    return data


def limpieza_dataset(data):
    data = data.dropna()
    return data


def obtener_columnas(columnas, data):
    columnas = data.iloc[:, columnas]
    return columnas

def masmuertes(datamuertos):
    muertes = []
    gente = []
    indices = []
    porcentajes = []
    datos = []
    finalpaises = []
    finalmuertos = []
    posicion = []
    gente = datamuertos['Population'].tolist()
    muertes = datamuertos['Deaths'].tolist()
    i = 0
    for dato in gente:
        temp1 = muertes[i]/100
        temp2 = dato*temp1
        porcentajes.append(temp2)
        i = i + 1
    for i in range(5):
        maximo = max(porcentajes)
        temp = porcentajes.index(maximo)
        indices.append(temp)
        datos.append(maximo)
        porcentajes[temp] = 0
    j = 0
    for indice in indices:
        posicion.append(j+1)
        finalpaises.append(datamuertos.iloc[indice][0])
        finalmuertos.append(datos[j])
        j = j + 1
    dfmuertos = pd.DataFrame({"Posición" : posicion,
                              "Países" : finalpaises,
                              "Muertes" : finalmuertos})
    return dfmuertos


def main():
    data = cargar_dataset()
    data = limpieza_dataset(data)
    datamuertos = obtener_columnas([0, 27, 30], data)
    dfmuertos = masmuertes(datamuertos)
    fig = dfmuertos["Muertes"].plot()
    plt.savefig("imagen.png")
    plt.close()
    return dfmuertos

if __name__ == "__main__":
    main()

