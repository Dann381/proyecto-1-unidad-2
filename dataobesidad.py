import os
import pandas as pd
import matplotlib.pyplot as plt


def cargar_dataset():
    data = pd.read_csv("suministro_alimentos_kcal.csv")
    return data


def limpieza_dataset(data):
    data = data.dropna()
    return data


def obtener_columnas(columnas, data):
    columnas = data.iloc[:, columnas]
    return columnas


def dataobesidad(obesidad):
    paises = []
    finalobesos = []
    finalgente = []
    obesos = []
    gente = []
    indices = []
    porcentajes = []
    datos = []
    posicion = []
    gente = obesidad['Population'].tolist()
    obesos = obesidad['Obesity'].tolist()
    i = 0
    for dato in gente:
        temp1 = obesos[i]/100
        temp2 = dato*temp1
        porcentajes.append(temp2)
        i = i + 1
    for i in range(5):
        maximo = max(porcentajes)
        datos.append(maximo)
        temp = porcentajes.index(maximo)
        indices.append(temp)
        porcentajes[temp] = 0
    j = 0
    for indice in indices:
        posicion.append(j+1)
        paises.append(obesidad.iloc[indice][0])
        finalobesos.append(datos[j])
        finalgente.append(obesidad.iloc[indice][2])
        j = j + 1
    dfobesidad = pd.DataFrame({"Posición" : posicion,
                               "Países" : paises,
                               "Obesidad" : finalobesos,
                               "Habitantes" : finalgente})
    return dfobesidad


def main():
    data = cargar_dataset()
    data = limpieza_dataset(data)
    obesidad = obtener_columnas([0, 24, 30], data)

    dfobesidad = dataobesidad(obesidad)
    fig = dfobesidad["Obesidad"].plot(kind="hist")
    plt.savefig("imagen.png")
    plt.close()
    return dfobesidad


if __name__ == "__main__":
    main()

