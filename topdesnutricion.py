import os
import pandas as pd
import matplotlib.pyplot as plt


def cargar_dataset():
    data = pd.read_csv("suministro_alimentos_kcal.csv")
    return data


def limpieza_dataset(data):
    data = data.dropna()
    return data


def obtener_columnas(columnas, data):
    columnas = data.iloc[:, columnas]
    return columnas


def topdesnutricion(desnutricion):
    flacos = []
    gente = []
    indices = []
    porcentajes = []
    datos = []
    finalpaises = []
    finaldesnutricion = []
    posicion = []
    gente = desnutricion['Population'].tolist()
    flacos = desnutricion['Undernourished'].tolist()
    i = 0
    for dato in gente:
        if flacos[i] == '<2.5':
            flacos[i] = 2.4
        flacos[i] = float(flacos[i])
        temp1 = flacos[i]/100
        temp2 = dato*temp1
        porcentajes.append(temp2)
        i = i + 1
    for i in range(5):
        maximo = max(porcentajes)
        temp = porcentajes.index(maximo)
        indices.append(temp)
        datos.append(maximo)
        porcentajes[temp] = 0
    i = 0
    for indice in indices:
        posicion.append(i+1)
        finalpaises.append(desnutricion.iloc[indice][0])
        finaldesnutricion.append(datos[i])
        i = i + 1
    dfdesnutricion = pd.DataFrame({"Posición" : posicion,
                                   "Países" : finalpaises,
                                   "Desnutricion" : finaldesnutricion})
    return dfdesnutricion


def main():
    data = cargar_dataset()
    data = limpieza_dataset(data)
    desnutricion = obtener_columnas([0, 25, 30], data)

    dfdesnutricion = topdesnutricion(desnutricion)
    fig = dfdesnutricion["Desnutricion"].plot(kind="bar")
    plt.savefig("imagen.png")
    plt.close()
    return dfdesnutricion


if __name__ == "__main__":
    main()

