import os
import pandas as pd
import matplotlib.pyplot as plt


def cargar_dataset():
    data = pd.read_csv("suministro_alimentos_kcal.csv")
    return data


def limpieza_dataset(data):
    data = data.dropna()
    return data


def obtener_columnas(columnas, data):
    columnas = data.iloc[:, columnas]
    return columnas

def obesidad_muertos(obsmuerte):
    ptjeobesos = []
    obesos = []
    gente = []
    ptjecmuertos = []
    muertos = []
    indices = []
    suma = []
    paises = []
    finalobesidad = []
    finalmuertos = []
    posicion = []
    gente = obsmuerte['Population'].tolist()
    ptjeobesos = obsmuerte['Obesity'].tolist()
    ptjemuertos = obsmuerte['Deaths'].tolist()
    i = 0
    for dato in gente:
        temp = ptjeobesos[i]/100
        temp2 = ptjemuertos[i]/100
        temp3 = dato*temp
        temp4 = dato*temp2
        obesos.append(temp3)
        muertos.append(temp4)
        i = i + 1
    i = 0
    for numero in obesos:
        suma.append(numero+muertos[i])
        i = i + 1
    for i in range(5):
        maximo = max(suma)
        temp = suma.index(maximo)
        indices.append(temp)
        suma[temp] = 0
    i = 0
    for indice in indices:
        posicion.append(i+1)
        paises.append(obsmuerte.iloc[indice][0])
        finalobesidad.append(obesos[indice])
        finalmuertos.append(muertos[indice])
        i = i + 1
    dfobsmuertos = pd.DataFrame({"Posición" : posicion,
                                 "Países" : paises,
                                 "Obesidad" : finalobesidad,
                                 "Muertos" : finalmuertos})
    return dfobsmuertos

def main():
    data = cargar_dataset()
    data = limpieza_dataset(data)
    obsmuerte = obtener_columnas([0, 24, 27, 30], data)

    dfobsmuertos = obesidad_muertos(obsmuerte)
    fig = dfobsmuertos.plot.scatter(x = "Obesidad", y = "Muertos")
    plt.savefig("imagen.png")
    plt.close()

    return dfobsmuertos


if __name__ == "__main__":
    main()

