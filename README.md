==== Proyecto 1 ====

Este proyecto consta de una serie de funciones orientadas al análisis de un Dataset que contiene una serie de datos relacionados a la dieta de una lista de países, y cifras de los mismos países respecto a su estado frente a la pandemia producida por el Covid-19.
Se han propuesto cinco hipótesis o interrogantes a responder antes de analizar el Dataset, para luego poder mostrar los resultados obtenidos mediante el uso de una interfaz gráfica creada con Glade. Estas interrogantes son:

1.- ¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad?

2.- ¿Cuáles son los 5 paı́ses que tienen más habitantes con obesidad y que además lo que más muertes tienen?

3.- ¿Los paı́ses que tienen mayor consumo de alcohol son los paı́ses que más contagios confirmados tienen?

4.- ¿Cuáles son los 5 paı́ses que tienen más habitantes con desnutrición?

5.- ¿Cuáles son los 5 paı́ses que tienen más muertes por Covid-19?

La respuesta a estas interrogantes se visualizan en el mismo programa, mediante dataframes y gráficos para ejemplificar de mejor manera el análisis de los datos

==== Pre-Requisitos y Ejecución del Programa ====

Tener instalado Python 3


Tener instalado el repositorio Pandas


Tener instalado GTK+3.0

Para ejecutar este proyecto se debe abrir el archivo mainwindow.py con Python 3. Una vez abierto, se abrirá una ventana la cual contiene un ComboboxText con las 5 interrogantes que el usuario puede seleccionar, además de un botón que abrirá una ventana de dialogo con el nombre de los desarrolladores. Al seleccionar alguna pregunta, se mostrará un ListStore con los datos correspondientes a la pregunta, y en la parte inferior se mostrará un GtkImage que contiene el gráfico creado por la respectiva hipótesis al momento de seleccionarla. Al inferior del gráfico se mostrará un label con la posición y el nombre del país que el usuario seleccione en el ListStore.

==== Funcionamiento ====

Este proyecto consta de 7 archivos .py, un archivo con el dataset, y una carpeta que contiene el archivo .ui con la interfaz.
Cinco de los archivos .py están encargados de crear un dataframe y una imágen por cada pregunta creada. Es decir, por las cinco preguntas que hemos propuesto, hemos creado cinco archivos distintos para obtener un análisis más ordenado.
Los otros dos archivos restantes están encargados de controlar la interfaz gráfica, y de mostrar los dataframes e imagenes obtenidos al procesar el dataset.
Por último, el archivo .ui contiene toda la parte gráfica, posee una ventana principal con el ComboboxText, un espacio que contiene el GtkTreeView y otro que contiene al GtkImage, un label que muestra la posición y el país que el usuario seleccione en el ListStore, y un botón que abre otra ventana de tipo dialogo con los nombres de los desarrolladores de este proyecto.


==== CONSTRUIDO CON ====

Vim - the ubiquitous text editor
Glade - A User Interface Designer

==== Desarrollo ====

Este programa fue desarrollado por Daniel Tobar y José Valenzuela 
