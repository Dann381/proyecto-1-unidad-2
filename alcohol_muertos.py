import os
import pandas as pd
import matplotlib.pyplot as plt


def cargar_dataset():
    data = pd.read_csv("suministro_alimentos_kcal.csv")
    return data


def limpieza_dataset(data):
    data = data.dropna()
    return data


def obtener_columnas(columnas, data):
    columnas = data.iloc[:, columnas]
    return columnas


def alcohol_muertos(df):
    alcohol = []
    ptjeactivos = []
    activos = []
    gente = []
    indices = []
    topalcohol = []
    topactivos = []
    datostopactivos = []
    finalpalcohol = []
    finalalcohol = []
    finalpactivos = []
    finalactivos = []
    posicion = []
    gente = df['Population'].tolist()
    alcohol = df['Alcoholic Beverages'].tolist()
    ptjeactivos = df['Active'].tolist()
    i = 0
    for dato in gente:
        temp1 = ptjeactivos[i]/100
        temp2 = dato*temp1
        activos.append(temp2)
        i = i + 1
    for i in range(10):
        maxalcohol = max(alcohol)
        maxactivos = max(activos)
        temp1 = alcohol.index(maxalcohol)
        temp2 = activos.index(maxactivos)
        datostopactivos.append(maxactivos)
        topalcohol.append(temp1)
        topactivos.append(temp2)
        alcohol[temp1] = 0
        activos[temp2] = 0
    for indice in topalcohol:
        finalpalcohol.append(df.iloc[indice][0])
        finalalcohol.append(df.iloc[indice][1])
    i = 0
    for indice in topactivos:
        posicion.append(i+1)
        finalpactivos.append(df.iloc[indice][0])
        finalactivos.append(datostopactivos[i])
        i = i + 1
    data = pd.DataFrame({"Posición" : posicion,
                         "Países Alcohol" : finalpalcohol,
                         "Cantidad Alcohol" : finalalcohol,
                         "Países Covid-19" : finalpactivos,
                         "Casos Activos" : finalactivos})
    return data

def main():
    data = cargar_dataset()
    data = limpieza_dataset(data)
    df = obtener_columnas([0, 1, 29, 30], data)

    dfalcohol = alcohol_muertos(df)
    fig = dfalcohol.plot.scatter(x="Cantidad Alcohol",
                                 y="Casos Activos")
    plt.savefig("imagen.png")
    plt.close()
    return dfalcohol


if __name__ == "__main__":
    main()

